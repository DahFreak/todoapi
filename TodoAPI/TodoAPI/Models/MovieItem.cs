﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoAPI.Models
{
    public class MovieItem
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Director { get; set; }
    }
}
