﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoAPI.Models
{
    public class MessageItem
    {
        public string Status { get; set; }
        public string[] Responses { get; set; }
        public int Sessionid { get; set; }
    }
}
