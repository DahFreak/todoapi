﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TodoAPI.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TodoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PandoraController : Controller
    {
        private static string appId = "1409617552558";
        private static string botName = "svsbot";
        private static string userKey = "cfb3de6daa80cd4b406b8215ed7266d8";
        //private static readonly HttpClient client = new HttpClient();

        //GET: api/<controller>
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    string x = AccessTheWebAsync().Result;
        //    return new string[] { x, "value2" };
        //}


        // Man kan returna items lite hur man vill (y)
        [HttpPost]
        public TodoItem Create(TodoItem item)
        {
            TodoItem todo = new TodoItem
            {
                Id = 1,
                Name = item.Name,
                IsComplete = true
            };

            return todo;
        }

        [HttpPost("test")]
        public TodoItem Testi(string message)
        {
            TodoItem todo = new TodoItem
            {
                Id = 1,
                Name = message,
                IsComplete = true
            };

            return todo;
        }

        [HttpPost("tmp")]
        public TodoItem Tmp([FromBody] TodoItem item)
        {
            return item;
        }

        //private static async Task<string> AccessTheWebAsync()
        //{
        //    Task<string> getStringTask = client.GetStringAsync("https://ghibliapi.herokuapp.com/films/2baf70d1-42bb-4437-b551-e5fed5a87abe");

        //    string urlContents = await getStringTask;

        //    string url = "https://aiaas.pandorabots.com/talk/1409617552558/svsbot?user_key=cfb3de6daa80cd4b406b8215ed7266d8&input=Hej jag har glömt mitt lösenord";
        //    string data = "Hej jag har glömt mitt lösenord";
        //    StringContent queryString = new StringContent(data);

        //    HttpResponseMessage response = await client.PostAsync(new Uri(url), queryString);

        //    response.EnsureSuccessStatusCode();
        //    string responseBody = await response.Content.ReadAsStringAsync();

        //    return responseBody;
        //}

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("https://aiaas.pandorabots.com");
                    client.DefaultRequestHeaders
                        .Accept
                        .Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var values = new Dictionary<string, string>
                    {
                       { "input", "jag har glömt mitt lösenord" },
                       { "user_key", "cfb3de6daa80cd4b406b8215ed7266d8" }
                    };

                    var content = new FormUrlEncodedContent(values);
                    
                    var response = await client.PostAsync($"/talk/1409617552558/svsbot", content);
                    response.EnsureSuccessStatusCode();

                    var stringResult = await response.Content.ReadAsStringAsync();
                    var x = JsonConvert.DeserializeObject<MessageItem>(stringResult);
                    //Den här skiten går att göra bättre
                    string json = JsonConvert.SerializeObject(new
                    {
                        results = new List<MessageItem>()
                        {
                            new MessageItem{Status = x.Status, Responses = x.Responses, Sessionid = x.Sessionid}
                        }
                    });
                    return Ok(new
                    {
                        json
                    //rawMessage.Status,
                    //rawMessage.Responses,
                    //rawMessage.SessionId
                });
                    //client.BaseAddress = new Uri("https://aiaas.pandorabots.com");
                    //client.DefaultRequestHeaders
                    //    .Accept
                    //    .Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    //HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, $"/talk/1409617552558/svsbot?user_key=cfb3de6daa80cd4b406b8215ed7266d8");
                    //request.Content = new StringContent("{\"input\":\"lösenord\",\"user_key\":cfb3de6daa80cd4b406b8215ed7266d8}",
                    //                Encoding.UTF8,
                    //                "application/json");
                    //string x = "";
                    //await client.SendAsync(request)
                    //    .ContinueWith(responseTask =>
                    //    {
                    //        x = responseTask.ToString();
                    //        // x = request.ToString();
                    //    });
                    //return Ok(new
                    //{
                    //    x,
                    //});


                    //StringContent stringContent = new StringContent(
                    //    "{ \"input\": \"Lösenord\" }, { \"user_key\": \"cfb3de6daa80cd4b406b8215ed7266d8\" }", UnicodeEncoding.UTF8, "application/json");

                    //var response = await client.PostAsync($"/talk/1409617552558/svsbot", stringContent);
                    ////var response = await client.GetAsync($"/talk/" + appId + "/" + botName + "?user_key=" + userKey + "&input=Hej, jag har glömt mitt lösenord");
                    //response.EnsureSuccessStatusCode();

                    //var stringResult = await response.Content.ReadAsStringAsync();
                    //var rawMessage = JsonConvert.DeserializeObject<MessageItem>(stringResult);

                    //return Ok(new
                    //{
                    //    rawMessage.Status,
                    //    rawMessage.Responses,
                    //    rawMessage.SessionId
                    //});

                    //                StringContent stringContent = new StringContent(
                    //"{ \"input\": \"Lösenord\" }, { \"user_key\": \"cfb3de6daa80cd4b406b8215ed7266d8\" }", UnicodeEncoding.UTF8, "application/json");

                    //                var response = await client.PostAsync($"/talk/1409617552558/svsbot?user_key=cfb3de6daa80cd4b406b8215ed7266d8", new StringContent("{\"input\":\"lösenord\",\"user_key\":cfb3de6daa80cd4b406b8215ed7266d8}",
                    //                    Encoding.UTF8,
                    //                    "application/json"));
                    //                //var response = await client.GetAsync($"/talk/" + appId + "/" + botName + "?user_key=" + userKey + "&input=Hej, jag har glömt mitt lösenord");
                    //                response.EnsureSuccessStatusCode();

                    //                var stringResult = await response.Content.ReadAsStringAsync();
                    //                var rawMessage = JsonConvert.DeserializeObject<MessageItem>(stringResult);

                    //                return Ok(new
                    //                {
                    //                    rawMessage.Status,
                    //                    rawMessage.Responses,
                    //                    rawMessage.SessionId
                    //                });

                }

                catch (HttpRequestException httpRequestException)
                {
                    return BadRequest($"Error getting message from Pandora: {httpRequestException.Message}");
                }
            }
        }
    }
}
