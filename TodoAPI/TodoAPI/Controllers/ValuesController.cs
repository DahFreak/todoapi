﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TodoAPI.Models;

namespace TodoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("https://ghibliapi.herokuapp.com");
                    var response = await client.GetAsync($"/films/2baf70d1-42bb-4437-b551-e5fed5a87abe");
                    response.EnsureSuccessStatusCode();

                    var stringResult = await response.Content.ReadAsStringAsync();
                    var rawMovie = JsonConvert.DeserializeObject<MovieItem>(stringResult);
                    var x = stringResult.GetType().ToString();
                    return Ok(new
                    {
                        x,
                        stringResult,
                         rawMovie.Title,
                         rawMovie.Description,
                         rawMovie.Director
                    });
                }

                catch (HttpRequestException httpRequestException)
                {
                    return BadRequest($"Error getting weather from GhibloMovies: {httpRequestException.Message}");
                }
            }
        }
    }
}
